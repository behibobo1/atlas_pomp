import { Inject, Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { GeneralService } from '../_services/general.service';
import { ProductDetails } from '../models/models';
import { TokenStorageService } from '../_services/token-storage.service';

@Injectable({
  providedIn: 'root',
})
export class ProductDetailsService {
  private httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      Authorization: `JWT ${this.tokenService.getToken()}`,
    }),
  };
  myAppUrl = '';
  constructor(
    private http: HttpClient,
    private tokenService: TokenStorageService,
    private gService: GeneralService,
    @Inject('BASE_URL') baseUrl: string
  ) {
    this.myAppUrl = baseUrl;
  }

  //   firstProductData: FirstProductsModel[] = [];
  private _groupItemsData = new BehaviorSubject<ProductDetails[]>([]); // Define as an array
  groupItemsData$: Observable<ProductDetails[]> =
    this._groupItemsData.asObservable();

  productData!: ProductDetails[];

  // getDetailsList(id: any) {
  //   // this._collection_id.next(id);
  //   return this.gService
  //     .getObservable<ProductDetails[]>(
  //       `store/products/${id}`,

  //       {}
  //     )
  //     .subscribe({
  //       next: (data) => {
  //         let res: any = data;
  //         this.productData = data;
  //         console.log(data);
  //         this._groupItemsData.next(data);
  //       },
  //       error() {},
  //     });
  // }
  getProductDetails(productId: string) {
    return this.gService.getObservable<ProductDetails[]>(
      `store/products/${productId}`,

      {}
    );
  }

  buy(product_id: any, quantity: any, cart_id: any) {
    const requestData = {
      product_id: product_id,
      quantity: quantity,
    };
    return this.http.post(
      this.myAppUrl + `store/carts/${cart_id}/items/`,
      requestData,
      {}
    );
  }
}
