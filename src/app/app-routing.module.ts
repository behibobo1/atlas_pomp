import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ProductDetailsComponent } from './product-details/product-details.component';
import { HomeComponent } from './home/home/home.component';
import { SignupComponent } from './signup/signup/signup.component';
import { SigninComponent } from './signin/signin.component';
import { ProfileComponent } from './profile/profile.component';
import { AuthGuard } from './_services/auth-gaurd.service';
import { ShoppingBasketComponent } from './shopping-basket/shopping-basket.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { OrderListComponent } from './order-list/order-list.component';
import { AboutUsComponent } from './about-us/about-us.component';
// import { ProductCardComponent } from './product-card/product-card/product-card.component';

export const routes: Routes = [
  { path: '', component: HomeComponent },
  {
    path: 'product-details/:id',
    component: ProductDetailsComponent,
    pathMatch: 'full',
  },
  { path: 'auth', component: SignupComponent, pathMatch: 'full' },
  { path: 'signin', component: SigninComponent, pathMatch: 'full' },
  { path: 'toast', component: SignupComponent, pathMatch: 'full' },
  {
    path: 'profile',
    component: ProfileComponent,
    canActivate: [AuthGuard],
    pathMatch: 'full',
  },
  {
    path: 'order-list',
    component: OrderListComponent,
    canActivate: [AuthGuard],
    pathMatch: 'full',
  },
  {
    path: 'basket',
    component: ShoppingBasketComponent,
    pathMatch: 'full',
  },
  {
    path: 'about-us',
    component: AboutUsComponent,
    pathMatch: 'full',
  },
  { path: '**', component: PageNotFoundComponent },
];
@NgModule({
  imports: [RouterModule.forRoot(routes, { scrollPositionRestoration: 'top' })],
  exports: [RouterModule],
})
export class AppRoutingModule {}
