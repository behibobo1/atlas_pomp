import { Injectable } from '@angular/core';
import { HttpHeaders } from '@angular/common/http';
import { GeneralService } from './general.service';
import { SignUp } from '../models/models';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  private httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' }),
  };

  constructor(private gService: GeneralService) {}

  login(username: string, password: string) {
    return this.gService.postObservable<any[]>(
      'auth/jwt/create',
      { username, password },
      {}
    );
  }

  register(first_name: any, last_name: any, username: any, password: any) {
    return this.gService.postObservable<SignUp[]>(
      'auth/users/',
      {
        first_name,
        last_name,
        username,
        password,
      },
      {}
    );
  }
}
