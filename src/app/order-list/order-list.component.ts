import { Component } from '@angular/core';
import { BehaviorSubject, Observable, timeout } from 'rxjs';
import { Order } from '../models/models';
import { Router } from '@angular/router';
import { OrderListService } from './order-list.service';

@Component({
  selector: 'app-order-list',
  templateUrl: './order-list.component.html',
  styleUrls: ['./order-list.component.scss']
})
export class OrderListComponent {
  constructor(
    private orderListService: OrderListService,
    private router: Router
  ) {}
  ngOnInit() {
    this.getOrders();
  }
  public _groupItemsData = new BehaviorSubject<Order[]>([]);
  groupItemsData$: Observable<Order[]> = this._groupItemsData.asObservable();
  basketData!: Order[];
  goHome() {
    this.router.navigate(['']);
  }
  getOrders() {
    this.orderListService.getOrders().subscribe({
      next: (data) => {
        let res: any = data;
        // this.productData = data;
        this.basketData = res;
       

        this._groupItemsData.next(res);
        // console.warn('res');
        // console.warn(this.groupItemsData$);
      },
      error: (error) => {
        // console.warn(error);
      },
    });
  }
}
