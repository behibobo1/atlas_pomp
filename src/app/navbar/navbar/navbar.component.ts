import { ChangeDetectorRef, Component } from '@angular/core';
import { Router } from '@angular/router';
import { startWith } from 'rxjs';
import { StartupService } from 'src/app/_services/startup.service';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { FormControl } from '@angular/forms';
import { ProductCardService } from 'src/app/product-card/product-card.service';
import { ViewportScroller } from '@angular/common';
import { ProductCardComponent } from 'src/app/product-card/product-card.component';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss'],
})
export class NavbarComponent {
  searchInput = new FormControl();
  myControl = new FormControl('');
  options: string[] = ['One', 'Two', 'Three'];
  filteredOptions!: Observable<string[]>;
  searchText: string = '';

  ngOnInit() {
    this.filteredOptions = this.myControl.valueChanges.pipe(
      startWith(''),
      map((value) => this._filter(value || ''))
    );
    this.searchText = '';
  }

  private _filter(value: string): string[] {
    const filterValue = value.toLowerCase();

    return this.options.filter((option) =>
      option.toLowerCase().includes(filterValue)
    );
  }
  constructor(
    private router: Router,
    private cartService: StartupService,
    private productCardService: ProductCardService,
    private scroller: ViewportScroller,
    private _productCardComponent: ProductCardComponent
  ) {
    this.cartService.cartData$.subscribe((data) => {
      this.cartData = data;
      // console.warn(`here is it!!!! ${this.cartData}`);
      // console.warn(this.cartData);
      // You can now use this.cartData in this component
    });
  }

  value: number = 1; // Initial value
  showDropdown: boolean = false;
  searchResults: string[] = [];
  productQuantities: { [productId: number]: number } = {};

  cartData: any; // Define a variable to hold the cart data

  isHomePage(): boolean {
    return this.router.url === '/';
  }

  isDropdownVisible: boolean = false;

  toggleDropdown() {
    this.isDropdownVisible = !this.isDropdownVisible;
  }
  profile() {
    this.router.navigate(['/profile']);
  }
  basket() {
    this.router.navigate(['/basket']);
  }
  aboutUs() {
    this.router.navigate(['/about-us']);
  }
  onInputChange(event: any) {
    
      this.productCardService.getSearchProductCardsList(event.value);
      this.searchText = event.value;
      this.searchResults = event.value;
  }

  productCardsData$: Observable<any> =
    this.productCardService.productCardsData$;

  // filter(event: Event): void {
  //   const target = event.target as HTMLInputElement;
  //   if (target) {
  //     const query = target.value;
  //     this.filteredItems = this.items.filter((item) =>
  //       item.toLowerCase().includes(query.toLowerCase())
  //     );
  //     this.cdr.detectChanges(); // Manually trigger change detection
  //   }
  // }
  // In your component.ts file
  isInputFocused: boolean = false;

  searchInputChange(event: any) {
    // Implement your search logic here and populate this.searchResults
    // Example:
    // this.searchResults = this.searchService.search(event.target.value);
  }

  onItemSelected(selectedItem: string) {
    // Handle the item selected in the dropdown
    // For example, you can set the input field value to the selected item.
  }

  goToProductCards() {
    this._productCardComponent.scrollTo();
  }


}
