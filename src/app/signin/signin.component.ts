import { Component, Inject, OnInit } from '@angular/core';
import { AuthService } from '../_services/auth.service';
import { GeneralService } from '../_services/general.service';
import { Router } from '@angular/router';
import { TokenStorageService } from '../_services/token-storage.service';
import { Token } from '../models/models';
import { ProfileService } from '../profile/profile.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.scss'],
})
export class SigninComponent implements OnInit {
  angForm!: FormGroup;
  isSuccessful = false;
  isSignUpFailed = false;
  errorMessage = '';
  tokenData!: Token[];
  private httpOptions = {
    headers: new HttpHeaders({
      Authorization: `JWT ${this.token.getToken()}`,
    }),
  };
  myAppUrl = '';

  constructor(
    private profileService: ProfileService,
    private authService: AuthService,
    private gService: GeneralService,
    private router: Router,
    private token: TokenStorageService,
    private http: HttpClient,
    private fb: FormBuilder, // Added FormBuilder
    @Inject('BASE_URL') baseUrl: string
  ) {
    this.myAppUrl = baseUrl;
  }

  ngOnInit(): void {
    this.createForm(); // Call createForm in ngOnInit
  }

  createForm() {
    this.angForm = this.fb.group({
      username: ['', [Validators.required, Validators.pattern(/\+989\d{9}/)]],
      password: ['', [Validators.required, Validators.minLength(8)]],
    });
  }

  onSubmit(): void {
    if (this.angForm.invalid) {
      this.angForm.markAllAsTouched();
      return;
    }

    const { username, password } = this.angForm.value;

    this.authService.login(username, password).subscribe({
      next: (data) => {
        this.tokenData = data;

        const dataArray = Array.isArray(this.tokenData)
          ? this.tokenData
          : [this.tokenData];

        const mappedData = dataArray.map((item) => {
          this.token.saveToken(item.access);
          
        this.profileService.getProfileDetailLogin(item.access).subscribe({
          next: (data: any) => {
            let res: any = data;
          },
          error: (error) => {
            this.gService.showErrorToastr(error.error.message);
          },
        });
        });

        this.getOrders();

        this.isSuccessful = true;
        this.isSignUpFailed = false;
        this.gService.showSuccessToastr('ورود شما با موفقیت انجام شد');

        this.router.navigate(['']);

      },
      error: (err) => {
        this.errorMessage = err.error.message;
        this.isSignUpFailed = true;
        this.gService.showErrorToastr(this.errorMessage);
      },
    });
  }

  getOrders() {
    return this.http.get<any[]>(
      this.myAppUrl + `store/orders/`,
      this.httpOptions
    );
  }
  // showRegularToast() {
  //   this.toastComponent.addToast('Title', 'Message', 'true');
  // }

  // // To show an error toast
  // showErrorToast(errorMessage: string): void {
  //   this.toastService.showError(errorMessage, 'Error');
  // }
}
