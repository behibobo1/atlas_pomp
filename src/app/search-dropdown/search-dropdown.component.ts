import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { StartupService } from 'src/app/_services/startup.service';

@Component({
  selector: 'search-dropdown',
  templateUrl: './search-dropdown.component.html',
  styleUrls: ['./search-dropdown.component.scss'],
})
export class SearchDropdownComponent {
  constructor(private router: Router, private cartService: StartupService) {}
  showDropdown: boolean = false;
  searchResults: string[] = []; // Replace with your search results data

  isHomePage() {
    // Implement your logic for checking if it's the home page
    return this.router.url === '/'; // Change to your logic
  }

  toggleDropdown() {
    this.showDropdown = !this.showDropdown;
  }

  searchInputChange(event: any) {
    // Implement your search logic here and populate this.searchResults
    // Example:
    // this.searchResults = this.searchService.search(event.target.value);
  }

  onItemSelected(selectedItem: string) {
    // Handle the item selected in the dropdown
    // For example, you can set the input field value to the selected item.
  }
}
