// auth.guard.ts
import { Injectable } from '@angular/core';
import {  Router } from '@angular/router';
import { TokenStorageService } from './token-storage.service'; // Import your TokenStorageService

@Injectable({
  providedIn: 'root',
})
export class AuthGuard {
  constructor(private tokenService: TokenStorageService, private router: Router) {}

  canActivate(): boolean {
    if (this.tokenService.isAuthenticated()) {
      return true;
    } else {
      this.router.navigate(['/signin']); // Redirect to the login page if not authenticated
      return false;
    }
  }
}
