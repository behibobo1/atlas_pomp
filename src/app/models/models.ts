export class FirstProductsModel {
  id!: number;
  title!: string;
  featured_group!: FeaturedGroup;
}

export class FeaturedGroup {
  id!: number;
  title!: string;
  collection!: number;
  image!: string;
}

export class CategoryModel {
  id!: number;
  title!: string;
  slug!: string;
  image!: string;
  collection!: number;
  description!: string;
  products!: Product[];
}
export class Product {
  id!: number;
  title!: string;
  unit_price!: number;
  images!: {
    id: number;
    image: string;
  }[];
}

export class ProductCard {
  id!: number;
  title!: string;
  description!: string;
  introduction!: string;
  powers!: Power;
  slug!: string;
  inventory!: number;
  unit_price!: number;
  cart_quantity!: number | null;
  group!: number;
  images!: Image[];
  attributes!: Attribute;
}

export class Power {
  id!: number;
  unit!: string;
  power!: number;
}

export class Image {
  id!: number;
  image!: string;
}

export class Attribute {
  id!: number;
  attribute!: string;
}

export class ProductDetails {
  id!: number;
  title!: string;
  description!: string;
  introduction!: string;
  powers!: string[]; // You can change the data type of powers based on the actual data type.
  slug!: string;
  inventory!: number;
  unit_price!: number;
  cart_quantity!: number;
  group!: number;
  images!: Image[];
  attributes!: any[]; // You can define a specific type for attributes based on your data.
}
export class SignUp {
  username!: any;
  first_name!: any;
  last_name!: any;
  password!: any;
}

export class Token {
  refresh!: string;
  access!: string;
}
export class Profile {
  username!: any;
  first_name!: any;
  last_name!: any;
  email!: any;
}

export class Order {
  id!: number;
  customer!: number;
  placed_at!: string;
  status!: OrderStatus[];
  last_status!: string;
  items!: OrderItem[];
  grand_total!: number; 
}

export class OrderStatus {
  id!: number;
  payment_status!: string;
  status_change!: string;
}

export class OrderItem {
  id!: number;
  product!: Products;
  unit_price!: number;
  quantity!: number;
  total_price!: number; 
}

export class Products {
  id!: number;
  title!: string;
  group!: number;
  images!: ProductImage[];
}

export class ProductImage {
  id!: number;
  image!: string;
}



export class Cart {
  id!: string;
  items!: CartItem[];
  grand_total!: number;
  customer: any;
}

export class CartItem {
  id!: number;
  product!: Product1;
  quantity!: number;
  total_price!: number;
}

export class Product1 {
  id!: number;
  title!: string;
  unit_price!: number;
  images!: ProductImage[];
}

