import { Inject, Injectable } from '@angular/core';
import { GeneralService } from '../_services/general.service';
import { BehaviorSubject, Observable } from 'rxjs';
import { CartIdStorageService } from '../_services/cart-id.service';
import { TokenStorageService } from '../_services/token-storage.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root',
})
export class ShoppingBasketService {
  private httpOptions = {
    headers: new HttpHeaders({
      Authorization: `JWT ${this.tokenService.getToken()}`,
    }),
  };
  myAppUrl = '';
  constructor(
    private gService: GeneralService,
    private cartId: CartIdStorageService,
    private http: HttpClient,
    private tokenService: TokenStorageService,
    @Inject('BASE_URL') baseUrl: string
  ) {
    this.myAppUrl = baseUrl;
  }

  finalOrders(id: any) {
    return this.http.post<any[]>(
      this.myAppUrl + `store/orders/`,
      {
        cart_id: id,
      },
      this.httpOptions
    );
  }
  delOrder(cartId: any, itemId: any) {
    return this.http.delete<any[]>(
      this.myAppUrl + `store/carts/${cartId}/items/${itemId}/`
    );
  }
  editOrder(num: any, cartId: any, itemId: any) {
    return this.http.patch<any[]>(
      this.myAppUrl + `store/carts/${cartId}/items/${itemId}/`,
      {
        quantity: num,
      },
      this.httpOptions
    );
  }

  getProfile() {
    return this.gService.update<any>('auth/users/me/', {}, this.httpOptions);
  }

  getComplete() {
    return this.http.get(
      this.myAppUrl + 'store/customers/me/',

      this.httpOptions
    );
  }
}
