import { Inject, Injectable } from '@angular/core';
import { GeneralService } from '../_services/general.service';
import { CartIdStorageService } from '../_services/cart-id.service';
import { TokenStorageService } from '../_services/token-storage.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root',
})
export class OrderListService {
  private httpOptions = {
    headers: new HttpHeaders({
      Authorization: `JWT ${this.tokenService.getToken()}`,
    }),
  };
  myAppUrl = '';
  constructor(
    private gService: GeneralService,
    private cartId: CartIdStorageService,
    private http: HttpClient,
    private tokenService: TokenStorageService,
    @Inject('BASE_URL') baseUrl: string
  ) {
    this.myAppUrl = baseUrl;
  }

  getOrders() {
    return this.http.get<any[]>(
      this.myAppUrl + `store/orders/`,
      this.httpOptions
    );
  }
}
