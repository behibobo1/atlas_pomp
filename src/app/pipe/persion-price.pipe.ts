import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'persianPrice'
})
export class PersianPricePipe implements PipeTransform {
  transform(value: number): string {
    if (isNaN(value)) {
      return value.toString();
    }

    const persianPrice = value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
    return ` ${persianPrice} تومان`;
  }
}

@Pipe({
  name: 'persianPrice0'
})
export class PersianPrice0Pipe implements PipeTransform {
  transform(value: number): string {
    if (isNaN(value)) {
      return value.toString();
    }

    const persianPrice0 = value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
    return ` ${persianPrice0}`;
  }
}

