import {
  APP_INITIALIZER,
  NgModule,
} from '@angular/core';
import {
  BrowserModule,
  provideClientHydration,
} from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NavbarComponent } from './navbar/navbar/navbar.component';
import { CarouselComponent } from './carousel/carousel/carousel.component';
import { FirstProductsComponent } from './first-products/first-products/first-products.component';
import { FirstProductsService } from './first-products/service-first-products.service';
import { GeneralService } from './_services/general.service';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { ToastrModule, ToastrService } from 'ngx-toastr';
import { environment } from 'src/environments/environment';
import { GroupItemsComponent } from './group-items/group-items/group-items.component';
import { GroupItemsService } from './group-items/service-group-items.service';
import { FormsModule } from '@angular/forms';
import { FooterComponent } from './footer/footer/footer.component';
import { ProductCardComponent } from './product-card/product-card.component';
import { ProductCardService } from './product-card/product-card.service';
import { ProductDetailsComponent } from './product-details/product-details.component';
import { HomeComponent } from './home/home/home.component';
import { PersianPrice0Pipe, PersianPricePipe } from './pipe/persion-price.pipe';
import { OrderStatusPipe, OrderStatusPercentagePipe } from './pipe/order-status';
import { SignupComponent } from './signup/signup/signup.component';
import { SigninComponent } from './signin/signin.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ProfileComponent } from './profile/profile.component';
import { StartupService } from './_services/startup.service';
import { ShoppingBasketComponent } from './shopping-basket/shopping-basket.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { OrderListComponent } from './order-list/order-list.component';
import { NgxPrintModule } from 'ngx-print';
import { AboutUsComponent } from './about-us/about-us.component';
import { SearchDropdownComponent } from './search-dropdown/search-dropdown.component';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
import { ReactiveFormsModule } from '@angular/forms';

export function startupServiceFactory(
  startupService: StartupService
): Function {
  return () => {
    setTimeout(() => {
      startupService.load();
    }, 3000);
  };
}

@NgModule({
  declarations: [
    PersianPrice0Pipe,
    PersianPricePipe,
    OrderStatusPipe,
    OrderStatusPercentagePipe,
    AppComponent,
    NavbarComponent,
    CarouselComponent,
    FirstProductsComponent,
    GroupItemsComponent,
    FooterComponent,
    ProductCardComponent,
    ProductDetailsComponent,
    HomeComponent,
    SignupComponent,
    SigninComponent,
    ProfileComponent,
    ShoppingBasketComponent,
    PageNotFoundComponent,
    OrderListComponent,
    AboutUsComponent,
    SearchDropdownComponent,
  ],
  imports: [
    ReactiveFormsModule,
    MatSelectModule,
    MatAutocompleteModule,
    MatInputModule,
    MatFormFieldModule,
    NgxPrintModule,
    FormsModule,
    BrowserAnimationsModule,
    FormsModule,
    BrowserModule,
    AppRoutingModule,
    NgbModule,
    HttpClientModule,
    ToastrModule.forRoot({
      timeOut: 10000, // Set the timeout for notifications (in milliseconds)
      positionClass: 'toast-top-right', // Set the default position of notifications
      // preventDuplicates: true, // Prevent duplicate notifications
      // closeButton: true, // Show a close button in notifications
      // progressBar: true, // Show a progress bar in notifications
      // progressAnimation: 'increasing', // Set the progress animation style ('increasing', 'decreasing', 'none')
      // tapToDismiss: true, // Allow dismissing notifications by clicking on them
      // newestOnTop: true, // Show newest notifications on top
      // enableHtml: true, // Enable HTML content in notifications
      // // rtl: false, // Set to true for right-to-left language support
      // maxOpened: 5, // Set the maximum number of opened notifications at a time
      // autoDismiss: true,
    }),
  ],
  providers: [
    // {
    //   provide: APP_INITIALIZER_API,
    //   useFactory: initializeApi,
    //   deps: [HttpClient],
    //   multi: true,
    // },
    {
      // Provider for APP_INITIALIZER
      provide: APP_INITIALIZER,
      useFactory: startupServiceFactory,
      deps: [StartupService],
      multi: true,
    },
    ProductCardComponent,
    ProductCardService,
    provideClientHydration(),
    GroupItemsComponent,
    GroupItemsService,
    FirstProductsComponent,
    FirstProductsService,
    GeneralService,
    ToastrService,

    {
      provide: 'BASE_URL',
      useValue: environment.apiRoot,
    },
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
