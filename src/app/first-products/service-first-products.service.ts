import { Injectable } from '@angular/core';
import { GeneralService } from '../_services/general.service';
import { FirstProductsModel } from '../models/models';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class FirstProductsService {
  constructor(private gService: GeneralService) {}

  //   firstProductData: FirstProductsModel[] = [];
  private _firstProductData = new BehaviorSubject<FirstProductsModel[]>([]); // Define as an array
  firstProductData = this._firstProductData.asObservable();

  getProductsList() {
    return this.gService.getObservable<FirstProductsModel[]>(
      'store/mainpage/',
      {}
    );
  }
}
