import { Injectable } from '@angular/core';
import { CategoryModel, ProductCard } from '../models/models';
import { GeneralService } from '../_services/general.service';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class ProductCardService {
  constructor(private gService: GeneralService) {}

  //   firstProductData: FirstProductsModel[] = [];
  private _groupItemsData = new BehaviorSubject<ProductCard[]>([]); // Define as an array
  productCardsData$: Observable<ProductCard[]> =
    this._groupItemsData.asObservable();

  public _collection_id = new BehaviorSubject<number | undefined>(undefined);
  collection_id$: Observable<number | undefined> =
    this._collection_id.asObservable();

  productData!: ProductCard[];

  private productBuyDataSubject: BehaviorSubject<any> = new BehaviorSubject<any>(null);
  productBuyData$: Observable<any> = this.productBuyDataSubject.asObservable();






  getProductCardsList(id: any) {
    if (id != undefined) {
      // this._collection_id.next(id);
      return this.gService
        .getObservable<ProductCard[]>(
          `store/products/?group_id=${id}`,

          {}
        )
        .subscribe({
          next: (data) => {
            let res: any = data;
            this.productData = data;
            this._groupItemsData.next(data);
          },
          error() {},
        });
    } else {
      return this.gService
        .getObservable<ProductCard[]>(`store/products/?group_id=`, {})
        .subscribe({
          next: (data) => {
            let res: any = data;
            this.productData = data;
            this._groupItemsData.next(data);

          },
          error() {},
        });
    }
  }


  getSearchProductCardsList(name: any) {

      // this._collection_id.next(id);
      return this.gService
        .getObservable<ProductCard[]>(
          `/store/products/?search=${name}`,

          {}
        )
        .subscribe({
          next: (data) => {
            let res: any = data;
            this.productData = data;
            this._groupItemsData.next(data);
          },
          error() {},
        });
   
  }
}
