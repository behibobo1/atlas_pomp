import { Injectable } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';
import { JwtHelperService } from '@auth0/angular-jwt';

const CART_ID = 'cart-id';

@Injectable({
  providedIn: 'root',
})
export class CartIdStorageService {
  constructor(private cookieService: CookieService) {}

  deleteCartId(): void {
    this.cookieService.delete(CART_ID, '/');

  }

  public saveCartId(catId: string): void {
    this.cookieService.set(CART_ID, catId,4);
  }

  public getCartId(): string | null {
    return this.cookieService.get(CART_ID) || null;
  }

  public cartIdExists(): boolean {
    // get the token

    const catId = this.getCartId();

    if (catId == null) {
      return false;
    }

    // return !this.jwtHelper.isTokenExpired(catId);
    return true;
  }
}
