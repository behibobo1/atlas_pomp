import { Component } from '@angular/core';
import { FirstProductsService } from '../service-first-products.service';
import { FirstProductsModel } from 'src/app/models/models';
import { Observable } from 'rxjs';
import { GroupItemsComponent } from 'src/app/group-items/group-items/group-items.component';
import { GroupItemsService } from 'src/app/group-items/service-group-items.service';

@Component({
  selector: 'app-first-products',
  templateUrl: './first-products.component.html',
  styleUrls: ['./first-products.component.scss'],
})
export class FirstProductsComponent {
  constructor(
    private _firstProductsService: FirstProductsService,
    private _groupItemsService: GroupItemsService,
    private _itemsCom: GroupItemsComponent
  ) {}

  firstProductData!: Observable<FirstProductsModel[]>;
  productData!: FirstProductsModel[];

  ngOnInit() {
    this.getData();
  }

  getData() {
    this._firstProductsService.getProductsList().subscribe({
      next: (data) => {
        this.productData = data;
        // console.log(data);
      },
      error(error) {
        // console.error('An error occurred:', error);
      },
    });
  }

  goToGroupItems(collection_id: number) {
    // this._groupItemsService.getGroupItemsList(collection_id);
    // this._itemsCom.getItems(collection_id);
    this._groupItemsService.getGroupItemsList(collection_id);
    this._itemsCom.scrollTo();
  }
}
