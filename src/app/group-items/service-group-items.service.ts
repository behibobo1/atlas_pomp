import { Injectable } from '@angular/core';
import { GeneralService } from '../_services/general.service';
import { CategoryModel } from '../models/models';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class GroupItemsService {
  constructor(private gService: GeneralService) {}

  //   firstProductData: FirstProductsModel[] = [];
  private _groupItemsData = new BehaviorSubject<CategoryModel[]>([]); // Define as an array
  groupItemsData$: Observable<CategoryModel[]> =
    this._groupItemsData.asObservable();

  public _collection_id = new BehaviorSubject<number | undefined>(undefined);
  collection_id$: Observable<number | undefined> =
    this._collection_id.asObservable();

  productData!: CategoryModel[];

  getGroupItemsList(id: any) {
    if (id != undefined) {
      // this._collection_id.next(id);
      return this.gService
        .getObservable<CategoryModel[]>(
          `store/groups/?collection_id=${id}`,

          {}
        )
        .subscribe({
          next: (data) => {
            let res: any = data;
            this.productData = data;
            this._groupItemsData.next(data);
          },
          error() {},
        });
    } else {
      return this.gService
        .getObservable<CategoryModel[]>(`store/groups/?collection_id=`, {})
        .subscribe({
          next: (data) => {
            let res: any = data;
            this.productData = data;
            this._groupItemsData.next(data);
          },
          error() {},
        });
    }
  }
}
