import { Injectable } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';
import { JwtHelperService } from '@auth0/angular-jwt';
import { Router } from '@angular/router';

const TOKEN_KEY = 'auth-token';
const USER_KEY = 'auth-user';

@Injectable({
  providedIn: 'root',
})
export class TokenStorageService {
  constructor(private cookieService: CookieService, private router: Router) {}
  jwtHelper = new JwtHelperService();
  signOut(): void {
    this.cookieService.delete(TOKEN_KEY);

    this.router.navigate(['']);
    // this.cookieService.delete(USER_KEY);
  }

  public saveToken(token: string): void {
    this.cookieService.set(TOKEN_KEY, token);
  }

  public getToken(): string | null {
    return this.cookieService.get(TOKEN_KEY) || null;
  }

  public saveUser(user: any): void {
    this.cookieService.set(USER_KEY, JSON.stringify(user));
  }

  public getUser(): any {
    const userString = this.cookieService.get(USER_KEY);
    return userString ? JSON.parse(userString) : {};
  }

  public isAuthenticated(): boolean {
    // get the token

    const token = this.getToken();

    if (token == null) {
      return false;
    }

    return !this.jwtHelper.isTokenExpired(token);
  }
}
