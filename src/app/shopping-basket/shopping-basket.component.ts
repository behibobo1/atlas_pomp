import { Component, Inject } from '@angular/core';
import { ShoppingBasketService } from './shopping-basket.service';
import { BehaviorSubject, Observable, timeout } from 'rxjs';
import { Cart, Order } from '../models/models';
import { Router } from '@angular/router';
import { StartupService } from '../_services/startup.service';
import { HttpClient } from '@angular/common/http';
import { CartIdStorageService } from '../_services/cart-id.service';
import * as jalaliMoment from 'jalali-moment';
import { GeneralService } from '../_services/general.service';
import { ModalDismissReasons, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { TokenStorageService } from '../_services/token-storage.service';

@Component({
  selector: 'app-shopping-basket',
  templateUrl: './shopping-basket.component.html',
  styleUrls: ['./shopping-basket.component.scss'],
})
export class ShoppingBasketComponent {
  myAppUrl: string;
  constructor(
    private basketService: ShoppingBasketService,
    private modalService: NgbModal,
    private router: Router,
    private http: HttpClient,
    private gService: GeneralService,
    private tokenService: TokenStorageService,
    private cartIdService: CartIdStorageService,
    @Inject('BASE_URL') baseUrl: string
  ) {
    this.myAppUrl = baseUrl;
  }

  currentPersianDate!: string;
  closeResult = '';
  isAuthenticated!: boolean;

  ngOnInit() {
    this.getBasket();
    this.currentPersianDate = jalaliMoment().format('jYYYY/jMM/jDD');
    this.isAuthenticated = this.tokenService.isAuthenticated();
    this.getProfileData();
    this.getComplete();
  }

  public _groupItemsData = new BehaviorSubject<Order[]>([]);
  groupItemsData$: Observable<Order[]> = this._groupItemsData.asObservable();
  basketData!: Order[];

  public _cartData = new BehaviorSubject<Cart[]>([]);
  cartData$: Observable<Cart[]> = this._cartData.asObservable();

  profileData: any;
  isComplete!: boolean;
  catId = '';
  getBasket() {
    this.http
      .get(this.myAppUrl + `store/carts/${this.cartIdService.getCartId()}`)
      .subscribe({
        next: (data: any) => {
          // console.log(data);
          this._cartData.next([data]); // Wrap the object in an array
        },
        error(err) {
          // console.error(err);
        },
      });
  }

  goHome() {
    this.router.navigate(['']);
  }

  getOrders() {
    // console.warn('res');
    this.basketService.finalOrders(this.cartIdService.getCartId()).subscribe({
      next: (data) => {
        let res: any = data;
        // this.productData = data;
        this.basketData = res;
        // console.warn(res);
        this.cartIdService.deleteCartId();
        this.http.post(this.myAppUrl + 'store/carts/', {}, {}).subscribe({
          next: (data) => {
            let res: any = data;
            this.catId = res['id'];
            this.cartIdService.saveCartId(this.catId);
          },
          error(err) {},
        });
        this.goHome();

        // this._groupItemsData.next(res);
        // console.warn('res');
        // console.warn(this.groupItemsData$);
      },
      error: (error) => {
        // console.warn(error);
      },
    });
  }

  delOrder(itemId: number) {
    this.basketService
      .delOrder(this.cartIdService.getCartId(), itemId)
      .subscribe({
        next: (data) => {
          let res: any = data;
          this.getBasket();
        },
        error: (error) => {
          // console.warn(error);
        },
      });
  }

  printContent() {
    window.print(); // Trigger the browser's print dialog
  }

  editOrder(qty: number, itemId: number) {
    this.basketService
      .editOrder(qty, this.cartIdService.getCartId(), itemId)
      .subscribe({
        next: (data) => {
          let res: any = data;
          this.getBasket();
        },
        error: (error) => {
          // console.warn(error);
        },
      });
  }

  getProfileData() {
    console.warn(this.isAuthenticated);
    if (this.isAuthenticated == true) {
      this.basketService.getProfile().subscribe({
        next: (data: any) => {
          let res: any = data;
          this.profileData = data;
          // this.isComplete = data.is_complete;

          // console.warn(this.profileData);
        },
        error: (error) => {
          this.gService.showErrorToastr(error.error.message);
        },
      });
    } else {
    }
  }

  getComplete() {
    if (this.isAuthenticated == true) {
      this.basketService.getComplete().subscribe({
        next: (data: any) => {
          let res: any = data;

          this.isComplete = data.is_complete;

          // console.warn(this.profileData);
        },
        error: (error) => {
          this.gService.showErrorToastr(error.error.message);
        },
      });
    }
  }

  open(content: any) {
    this.modalService
      .open(content, { ariaLabelledBy: 'modal-basic-title' })
      .result.then(
        (result) => {
          this.closeResult = `Closed with: ${result}`;
        },
        (reason) => {
          this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
        }
      );
  }

  // Get dismiss reason
  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

  displayStyle = 'none';
  displayStyle2 = 'none';
  displayStyle3 = 'none';
  openPopup() {
    this.displayStyle = 'block';
  }
  closePopup() {
    this.displayStyle = 'none';
  }

  openPopup2() {
    this.displayStyle2 = 'block';
  }
  closePopup2() {
    this.displayStyle2 = 'none';
  }

  signinPopup() {
    this.displayStyle3 = 'block';
  }
  closeSigninPopup() {
    this.displayStyle3 = 'none';
  }

  goProfile() {
    this.router.navigate(['profile']);
  }

  goSignin() {
    this.router.navigate(['signin']);
  }
}
