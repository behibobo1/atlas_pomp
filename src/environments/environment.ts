export const environment = {
  production: false,

  // *** For Working In Local Server
  // webSite: 'http://192.168.5.11:9595/',
  // apiRoot: 'http://192.168.5.11:9595/api/api/',
  // hubRoot: 'http://192.168.5.11:9595/hubs/',
  // versionCheckURL: 'http://192.168.5.11:9595/version.json'

  // *** For Working In Localhost
  //  siteName: "لوکال هاست",
  //  webSite: 'https://localhost:44389/',
  apiRoot: 'http://213.176.29.118/api/',
  //  hubRoot: 'https://localhost:44389/hubs/',
  //  versionCheckURL: 'https://localhost:44389/version.json',
  //  mapApiKey: 'eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6Ijg2MmQyYWFmMDdiN2E0ODhmMTZmYjA0NmM4OTk0NWU3MTE3NWYyMDZlNzY4ZmNiYWI2MWEwZjVkMjQ4MmY3ZGE3ZmZmNGE0MmVmNWRhNmJhIn0.eyJhdWQiOiI2OTgyIiwianRpIjoiODYyZDJhYWYwN2I3YTQ4OGYxNmZiMDQ2Yzg5OTQ1ZTcxMTc1ZjIwNmU3NjhmY2JhYjYxYTBmNWQyNDgyZjdkYTdmZmY0YTQyZWY1ZGE2YmEiLCJpYXQiOjE1NzU5NTcwMTYsIm5iZiI6MTU3NTk1NzAxNiwiZXhwIjoxNTc4NDYyNjE2LCJzdWIiOiIiLCJzY29wZXMiOlsiYmFzaWMiXX0.XH3bOHQWw-NPHiI1bG57yI8rylWgZ_TRz81o8xCOzIHjMp_p9UtNUTRufJzuG5QZSRlq1rCLhZgLB9ZAcGLaySQ6fGKyx0Wkf4dJ8sK5hMAe2SIuwQseV68kkGnw_YwuQbYqY_VicBnGSbkAH96kICVD0RRHLMeX6qq_8HtA8L3JuM9YFeWYKTK1M6oi3F96n0CUis2iPT4hmsTkvZ_zLNTFYiJJpUAqypodsxPPPotxmJPvVicc_limBnWOAc0piAF9TtuzN8HzPx0qQQY5RRAdGtdUmC_UI78Z_DNkqeFjR4pi8CbKcT6IpWx9ff27OkC5ShhWNw-sj3MKDhy_BQ',

  // For Firebase JS SDK v7.20.0 and later, measurementId is optional
  //  firebaseConfig: {
  //     vapidKey: "BFdgrQzyQVxMlKTf7mBlOdgqsM53rvc7noFgFYpRKiHzf6qce6Z3tD0GJ-Lr0DDdZeRFErgT-CsMu7-8s5CD7ww",

  //     apiKey: "AIzaSyBnwwpYH8HCjDRBQn6fdZkRTnME1gPR1Es",
  //     authDomain: "ticketing-3bea9.firebaseapp.com",
  //     projectId: "ticketing-3bea9",
  //     storageBucket: "ticketing-3bea9.appspot.com",
  //     messagingSenderId: "1044874344137",
  //     appId: "1:1044874344137:web:6d7665599fcf3efa713f18",
  //     measurementId: "G-Q5EMKH2JDZ"
  //  }
};
