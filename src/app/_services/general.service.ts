import { Injectable, Inject } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable, BehaviorSubject } from 'rxjs';
import { ToastrService } from 'ngx-toastr';


@Injectable({
  providedIn: 'root',
})
export class GeneralService {
  myAppUrl = '';

  minDate: any = { year: 1280, month: 1, day: 1 };
  maxDate: any = { year: 1410, month: 12, day: 30 };

  private _DateTime = new BehaviorSubject<any>({});
  DateTime = this._DateTime.asObservable();

  serverTime: any;

  constructor(
    private http: HttpClient,
    private toastr: ToastrService,
    @Inject('BASE_URL') baseUrl: string
  ) {
    this.myAppUrl = baseUrl;
  }

  // getServerDateTime() {
  //   this.get('Home/getCurrentDateTime', {}).subscribe(
  //     (result: any) => {
  //       this.maxDate.year = result.persianDateYear;

  //       this._DateTime.next(result);

  //       this.serverTime = setInterval(() => {
  //         let time: any;
  //         this.DateTime.subscribe((res) => {
  //           time = res.adDate;
  //         });
  //         var timeObject = new Date(time);
  //         timeObject.setSeconds(timeObject.getSeconds() + 60);

  //         this._DateTime.next(timeObject.toString());
  //       }, 60000);
  //     },
  //     (error) => {
  //       console.log(error);
  //     }
  //   );
  // }

  post(path: string, data: any, paramObj: any) {
    // options
    const httpOptions = {
      params: {},
    };
    let params = new HttpParams();
    if (paramObj !== '' && paramObj !== undefined && paramObj !== null) {
      for (const property in paramObj) {
        if (paramObj.hasOwnProperty(property)) {
          params = params.append(property, paramObj[property].toString());
        }
      }
    }
    httpOptions.params = params;

    return this.http.post(this.myAppUrl + path, data, httpOptions);
  }

  postObservable<T>(path: string, data: any, paramObj: any): Observable<T> {
    // options
    const httpOptions = {
      params: {},
    };
    let params = new HttpParams();
    if (paramObj !== '' && paramObj !== undefined && paramObj !== null) {
      for (const property in paramObj) {
        if (paramObj.hasOwnProperty(property)) {
          params = params.append(property, paramObj[property]);
        }
      }
    }
    httpOptions.params = params;

    return this.http.post<T>(this.myAppUrl + path, data, httpOptions);
  }

  get(path: string, data: any, resType: string = 'json') {
    if (resType == 'json') {
      const httpOptions = {
        params: {},
      };
      httpOptions.params = data;
      return this.http.get(this.myAppUrl + path, httpOptions);
    } else {
      return this.http.get(this.myAppUrl + path, {
        params: data,
        responseType: 'text',
      });
    }
  }

  getObservable<T>(path: string, data: any): Observable<T> {
    const httpOptions = {
      params: {},
    };
    httpOptions.params = data;
    return this.http.get<T>(this.myAppUrl + path, httpOptions);
  }

  update<T>(path: string, data: any, paramObj: any) {
    const httpOptions = {
      params: {},
    };
    let params = new HttpParams();
    if (paramObj !== '' && paramObj !== undefined && paramObj !== null) {
      for (const property in paramObj) {
        if (paramObj.hasOwnProperty(property)) {
          params = params.append(property, paramObj[property].toString());
        }
      }
    }
    httpOptions.params = params;

    return this.http.put<T>(this.myAppUrl + path, data, paramObj);
  }

  delete(data: any, path: string) {
    const httpOptions = {
      params: {},
    };
    let params = new HttpParams();
    if (data !== '' && data !== undefined && data !== null) {
      for (const property in data) {
        if (data.hasOwnProperty(property)) {
          params = params.append(property, data[property].toString());
        }
      }
    }

    httpOptions.params = params;
    return this.http.delete(this.myAppUrl + path, httpOptions);
  }

  showSuccessToastr(msg: string) {
    this.toastr.success(msg);
  }

  showErrorToastr(msg: string = 'خطایی رخ داده است.', title: string = 'خطا!') {
    this.toastr.error(msg, 'خطا! ');
  }

  showInfoToastr(msg: string) {
    this.toastr.info(msg);
  }

  showWarningToastr(msg: string) {
    this.toastr.warning(msg);
  }

  // loadScript(url: string) {
  //   const isFound = false;
  //   const scripts = document.getElementsByTagName('script');

  //   // tslint:disable-next-line:prefer-for-of
  //   for (let i = 0; i < scripts.length; ++i) {
  //     if (scripts[i] !== null) {
  //       const s = scripts[i];
  //       const a = s.getAttribute('src');

  //       if (a != null) {
  //         if (a.includes(url)) {
  //           document.getElementsByTagName('head')[0].removeChild(s);
  //         }
  //       }
  //     }
  //   }

  //   const dynamicScripts = [url];
  //   // tslint:disable-next-line:prefer-for-of
  //   for (let i = 0; i < dynamicScripts.length; i++) {
  //     const node = document.createElement('script');
  //     node.src = dynamicScripts[i];
  //     node.type = 'text/javascript';
  //     node.async = false;
  //     node.charset = 'utf-8';
  //     document.getElementsByTagName('head')[0].appendChild(node);
  //   }
  // }

  // loadScriptBlock(script: string) {
  //   const body = document.body as HTMLDivElement;
  //   const node = document.createElement('script');
  //   node.text = script;
  //   node.type = 'text/javascript';
  //   node.async = false;
  //   node.charset = 'utf-8';
  //   body.appendChild(node);
  // }

  // addClassToBody(className: string) {
  //   const body = document.body as HTMLDivElement;
  //   body.classList.add(className);
  // }

  // addClassToMainTag(className: string) {
  //   const main = document.querySelector(
  //     'main.tk_main_content'
  //   ) as HTMLDivElement;
  //   main.classList.add(className);
  // }

  // removeClassFromMainTag(className: string) {
  //   const main = document.querySelector(
  //     'main.tk_main_content'
  //   ) as HTMLDivElement;
  //   main.classList.remove(className);
  // }

  // addAttributeToBody(attrName: string, attrValue: string) {
  //   const body = document.body as HTMLDivElement;
  //   const att = document.createAttribute(attrName);
  //   att.value = attrValue;
  //   body.setAttributeNode(att);
  // }

  // numDigit(num: string) {
  //   let newNum = '';
  //   let count = 0;
  //   const len = num.toString().length;
  //   for (let index = len - 1; index >= 0; index--) {
  //     count++;
  //     newNum = num.toString()[index] + newNum;
  //     if (count == 3 && index > 0) {
  //       newNum = ',' + newNum;
  //       count = 0;
  //     }
  //   }
  //   return newNum;
  // }

  // replaceSpace(str: String) {
  //   return str.replace(/ /g, '_');
  // }

  // replaceUnderLine(str: String) {
  //   return str.replace(/_/g, ' ');
  // }

  // setSessionId() {
  //   return this.http.get(this.myAppUrl + 'Home/GetSessionId', {}).pipe(
  //     map((res: any) => {
  //       sessionStorage.setItem('sessionid', res);
  //     })
  //   );
  // }
}
