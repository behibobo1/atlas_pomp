import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ProductDetailsService } from './product-details.service';
import { StartupService } from '../_services/startup.service';
import { GeneralService } from '../_services/general.service';
import { CartIdStorageService } from '../_services/cart-id.service';

@Component({
  selector: 'app-product-details',
  templateUrl: './product-details.component.html',
  styleUrls: ['./product-details.component.scss'],
})
export class ProductDetailsComponent implements OnInit {
  product: any;
  showAllAttributes: boolean = false;
  value: number = 1; // Initial value

  increment() {
    this.value++;
  }

  decrement() {
    if (this.value > 1) {
      this.value--;
    }
  }
  constructor(
    private startupService: StartupService,
    private route: ActivatedRoute,
    private gService: GeneralService,
    private cartIdService: CartIdStorageService,
    private productService: ProductDetailsService
  ) {}

  ngOnInit() {
    const productId = this.route.snapshot.paramMap.get('id');
    if (productId) {
      // Fetch product details using the service
      this.productService.getProductDetails(productId).subscribe({
        next: (productDetails: any) => {
          let res: any = productDetails;
          this.product = productDetails;
          // console.warn(productDetails);
        },
        error: (error) => {},
      });
    }
  }
  toggleAttributes() {
    this.showAllAttributes = !this.showAllAttributes;
  }
  buy(product_id: any) {
    this.productService
      .buy(product_id, this.value, this.cartIdService.getCartId())
      .subscribe({
        next: (data: any) => {
          let res: any = data;

          this.startupService.getBasket();
          this.gService.showSuccessToastr('محصول به سبد خرید اضافه شد ');
        },
        error: (error) => {
          this.gService.showErrorToastr(error.error.message);
        },
      });
  }
}
