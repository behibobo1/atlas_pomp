import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/_services/auth.service';
import { GeneralService } from 'src/app/_services/general.service';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss'],
  animations: [
    // Your animations here
  ],
})
export class SignupComponent implements OnInit {
  angForm!: FormGroup; // Change 'form' to 'angForm'

  isSuccessful = false;
  isSignUpFailed = false;
  errorMessage = '';

  constructor(
    private authService: AuthService,
    private gService: GeneralService,
    private router: Router,
    private fb: FormBuilder // Add FormBuilder to the constructor
  ) {}

  ngOnInit(): void {
    this.createForm();
  }

  createForm(): void {
    this.angForm = this.fb.group({
      first_name: ['', [Validators.required, Validators.minLength(1), Validators.maxLength(20)]],
      last_name: ['', [Validators.required]],
      username: ['', [Validators.required, Validators.minLength(9), Validators.pattern(/\+989\d{9}/)]],
      password: ['', [Validators.required, Validators.minLength(8)]],
    });
  }

  onSubmit(): void {
    if (this.angForm.valid) {
      const { first_name, last_name, username, password } = this.angForm.value;

      this.authService
        .register(first_name, last_name, username, password)
        .subscribe({
          next: (data) => {
            this.isSuccessful = true;
            this.isSignUpFailed = false;
            this.gService.showSuccessToastr('اکانت شما با موفقیت ساخته شد');
            this.router.navigate(['/', 'signin']);
          },
          error: (err) => {
            this.errorMessage = err.error.message;
            this.isSignUpFailed = true;
            this.gService.showErrorToastr(this.errorMessage);
          },
        });
    }
  }
}
