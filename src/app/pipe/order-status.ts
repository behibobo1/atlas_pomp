import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'orderStatus'
})
export class OrderStatusPipe implements PipeTransform {
  transform(value: string): string {
    
    let result = "";

    switch(value) {
        case 'P':
          result = 'در انتظار تايید'
          break;
        case 'C':
					result = 'تايید شد'
          break;
				case 'F':
					result = 'ابطال'
          break;
				case 'S':
					result = 'در حال ارسال'
          break;
				case 'R':
					result = 'دریافت شد'
					break;
        default:
          // code block
      }

    return result;
  }
}


@Pipe({
  name: 'orderStatusPercentage'
})
export class OrderStatusPercentagePipe implements PipeTransform {
  transform(value: string): string {
    let result = "";

    switch(value) {
        case 'P':
          result = '25'
          break;
        case 'C':
					result = '50'
          break;
				case 'S':
					result = '75'
          break;
				case 'R':
					result = '100'
					break;
        default:
          // code block
      }

    return result;
  }
}

