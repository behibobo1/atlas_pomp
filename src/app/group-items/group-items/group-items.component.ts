import { Component, ChangeDetectorRef } from '@angular/core';
import { GroupItemsService } from '../service-group-items.service';
import { CategoryModel } from 'src/app/models/models';
import { ViewportScroller } from '@angular/common';
import { ProductCardComponent } from 'src/app/product-card/product-card.component';
import { ProductCardService } from 'src/app/product-card/product-card.service';

@Component({
  selector: 'app-group-items',
  templateUrl: './group-items.component.html',
  styleUrls: ['./group-items.component.scss'],
})
export class GroupItemsComponent {
  constructor(
    private _groupItemsService: GroupItemsService,
    private scroller: ViewportScroller,
    private _productCardComponent: ProductCardComponent, 
    private _productCardService: ProductCardService,
  ) {}

  productData: CategoryModel[] = [];


  ngOnInit() {
    this._groupItemsService.getGroupItemsList('');
    
  }
  groupItemsData$ = this._groupItemsService.groupItemsData$;
  

  scrollTo() {
    this.scroller.scrollToAnchor('scroll');
  }

  goToProductCards(id: any) {
    // this._groupItemsService.getGroupItemsList(collection_id);
    // this._itemsCom.getItems(collection_id);
    this._productCardService.getProductCardsList(id);
    this._productCardComponent.scrollTo()
  }


  // getItems(id: number) {
  //   this._groupItemsService.collection_id$;
  //   // this.counter = this.counter + 1;
  //   // console.log(this.counter);
  //   console.log(`getItems(id:number){${id}`);
  //   this._groupItemsService.getGroupItemsList(id).subscribe({
  //     next: (data) => {
  //       let res: any = data;
  //       this.productData = data;
  //       console.log(this.productData);
  //       // Trigger change detection manually

  //      },
  //     error() {},
  //   });
  // }
}
