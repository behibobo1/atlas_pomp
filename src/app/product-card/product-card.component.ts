import { ViewportScroller } from '@angular/common';
import { Component, EventEmitter, Input, Output } from '@angular/core';
import { ProductCardService } from './product-card.service';
import { Router } from '@angular/router';
import { ProductDetailsService } from '../product-details/product-details.service';
import { CartIdStorageService } from '../_services/cart-id.service';
import { GeneralService } from '../_services/general.service';
import { Observable } from 'rxjs';
import { StartupService } from '../_services/startup.service';

@Component({
  selector: 'app-product-card',
  templateUrl: './product-card.component.html',
  styleUrls: ['./product-card.component.scss'],
})
export class ProductCardComponent {
  constructor(
    private _productCardService: ProductCardService,
    private scroller: ViewportScroller,
    private router: Router,
    private cartIdService: CartIdStorageService,
    private gService: GeneralService,
    private productService: ProductDetailsService,private cartService: StartupService
  ) {}

  value: number = 1; // Initial value
  productQuantities: { [productId: number]: number } = {};


  cartData: any; // Define a variable to hold the cart data




  increment(productId: number) {
    if (this.productQuantities[productId] < 10) {
      this.productQuantities[productId]++;
    }
  }

  decrement(productId: number) {
    if (this.productQuantities[productId] > 1) {
      this.productQuantities[productId]--;
    }
  }

  ngOnInit() {

    
    this._productCardService.getProductCardsList('');

    this.productCardsData$.subscribe((products) => {
      for (const product of products) {
        this.productQuantities[product.id] = 1;
      }
    });
  }

  productCardsData$: Observable<any> =
    this._productCardService.productCardsData$;

  scrollTo() {
    this.scroller.scrollToAnchor('scroll1');
  }

  navigateToProductDetails(productId: number): void {
    this.router.navigate(['/product-details', productId]);
  }





  buy(product_id: number, qty: number = 1) {
    this.productService
      .buy(
        product_id,
        qty,
        this.cartIdService.getCartId()
      )
      .subscribe({
        next: (data: any) => {
          let res: any = data;
          this.cartService.getBasket();
          this.gService.showSuccessToastr('محصول به سبد خرید اضافه شد ');
        },
        error: (error) => {
          this.gService.showErrorToastr(error.error.message);
        },
      });
  }
}
