import { HttpClient } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { CartIdStorageService } from './cart-id.service';
import { BehaviorSubject, Observable } from 'rxjs';
import { Cart } from '../models/models';
const CART_ID = 'cart-id';
@Injectable({
  providedIn: 'root',
})
export class StartupService {
  myAppUrl = '';
  baseUrl: string | undefined;
  token!: String | null;
  catId = '';

  constructor(
    private http: HttpClient,
    private cartIdService: CartIdStorageService,
    @Inject('BASE_URL') baseUrl: string
  ) {
    this.myAppUrl = baseUrl;
  }

  public _cartData = new BehaviorSubject<Cart[]>([]);
  cartData$: Observable<Cart[]> = this._cartData.asObservable();

  load(): Promise<any> {
    return new Promise<void>((resolve, reject) => {
      if (this.cartIdService.cartIdExists() != true) {
        this.http.post(this.myAppUrl + 'store/carts/', {}, {}).subscribe({
          next: (data) => {
            let res: any = data;
            this.catId = res['id'];
            this.cartIdService.saveCartId(this.catId);

            resolve();
          },
          error(err) {
            resolve();
          },
        });
      }
      this.getBasket();
    });
  }




  
  getBasket() {
    if (this.cartIdService.getCartId() != null) {
      this.http
        .get(this.myAppUrl + `store/carts/${this.cartIdService.getCartId()}`)
        .subscribe({
          next: (data) => {
            let res: any = data;
            console.log('basket is here');
            this._cartData.next(res);
          },
          error(err) {},
        });
    }
  }
}
