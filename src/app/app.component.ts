import { Component, ViewChild } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'tabriz-atlas-pomp';

  @ViewChild('element') public element: any;
    public position = { X: 'Left', Y: 'Bottom' };

    onClick(e: any) {
      e.clickToClose = true;
    }

    onCreate(args: any) {
      this.element.show();
    }

    btnClick(args: any) {
      this.element.show();
    }
}
