import { Inject, Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Profile } from '../models/models';
import { GeneralService } from '../_services/general.service';
import { TokenStorageService } from '../_services/token-storage.service';

@Injectable({
  providedIn: 'root',
})
export class ProfileService {
  private httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      Authorization: `JWT ${this.tokenService.getToken()}`,
    }),
  };
  token = `JWT ${this.tokenService.getToken()}`;
  myAppUrl = '';

  constructor(
    private http: HttpClient,
    private tokenService: TokenStorageService,
    private gService: GeneralService,
    @Inject('BASE_URL') baseUrl: string
  ) {
    this.myAppUrl = baseUrl;
  }

  updateProfile(username: any, first_name: any, last_name: any, email: any) {
    return this.gService.update(
      'auth/users/me/',
      {
        username,
        first_name,
        last_name,
        email,
      },
      this.httpOptions
    );
  }
  updateProfileDetail(
    nationalid: any,
    state: any,
    city: any,
    adress: any,
    job: any,
    edu: any
  ) {
    return this.gService.update(
      'store/customers/me/',
      {
        nationalid,
        state,
        city,
        adress,
        job,
        edu,
      },
      this.httpOptions
    );
  }
  getProfileDetail() {
    return this.gService.update<Profile>(
      'store/customers/me/',
      {},
      this.httpOptions
    );
  }

  getProfileDetailLogin(ttoken: any) {
    return this.http.get<any>(
      'store/customers/me/',

      {
        headers: new HttpHeaders({
          Authorization: `JWT ${ttoken}`,
        }),
      }
    );
  }

  getProfile() {
    return this.gService.update<Profile>(
      'auth/users/me/',
      {},
      this.httpOptions
    );
  }
  changePhone(new_username: string, currentPassword: string) {
    const requestData = {
      new_username: new_username,
      current_password: currentPassword,
    };
    // console.warn(requestData);
    return this.http.post(
      this.myAppUrl + 'auth/users/set_username/',
      requestData,
      this.httpOptions
    );
  }

  // changePhone(new_username: any, current_password: any) {
  //   const requestData = {
  //     new_username: new_username,
  //     current_password: current_password,
  //   };
  //   return this.gService.postObservable(
  //     'auth/users/set_username',
  //     requestData,

  //     this.httpOptions
  //   );
  // }
  // changePass(current_password: any, new_password: any) {
  //   return this.gService.postObservable(
  //     'auth/users/set_password',
  //     {
  //       current_password,
  //       new_password,
  //     },
  //     {
  //       Authorization: `JWT ${this.tokenService.getToken()}`,
  //     }
  //   );
  // }

  changePass(current_password: any, new_password: any) {
    const requestData = {
      current_password: current_password,
      new_password: new_password,
    };
    // console.warn(requestData);
    return this.http.post(
      this.myAppUrl + 'auth/users/set_password/',
      requestData,
      this.httpOptions
    );
  }
}
